Name:           perl-B-Hooks-OP-Check
Summary:        Wrap OP check callbacks
Version:        0.22
Release:        18
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/B-Hooks-OP-Check
Source0:        https://cpan.metacpan.org/modules/by-module/B/B-Hooks-OP-Check-%{version}.tar.gz
BuildRequires:  coreutils
BuildRequires:  findutils
BuildRequires:  gcc
BuildRequires:  make
BuildRequires:  perl-devel
BuildRequires:  perl-generators
BuildRequires:  perl-interpreter
BuildRequires:  perl-ExtUtils-Depends >= 0.300 
BuildRequires:  perl(ExtUtils::MakeMaker) >= 6.76
BuildRequires:  perl(DynaLoader)
BuildRequires:  perl(parent)
BuildRequires:  perl(strict)
BuildRequires:  perl(warnings)
BuildRequires:  perl(File::Spec)
BuildRequires:  perl(Test::More)
BuildRequires:  perl(CPAN::Meta) >= 2.120900

%{?perl_default_filter}

%description
This module provides a C API for XS modules to hook into the callbacks
of 'PL_check'.

%prep
%setup -q -n B-Hooks-OP-Check-%{version}

%build
perl Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}" NO_PACKLIST=1 NO_PERLLOCAL=1
%{make_build}

%install
%{make_install}
%{_fixperms} -c %{buildroot}

%check
make test

%files
%license LICENCE
%doc Changes CONTRIBUTING README t/
%{perl_vendorarch}/auto/B/
%{perl_vendorarch}/B/
%{_mandir}/man3/B::Hooks::OP::Check.3*

%changelog
* Sun Jan 19 2025 Funda Wang <fundawang@yeah.net> - 0.22-18
- drop useless perl(:MODULE_COMPAT) requirement

* Wed Jun 29 2022 tanyulong <tanyulong@kylinos.cn> - 0.22-17
- init package for openEuler

